# Contest Vladislav-Camille

## Justification du MCD

- SecuHab >- AgencesFrance : 1 to many.
    - Une société principale pour plusieurs agence en France.
- AgencesFrance >- Plateforme : 1 to many.
    - Plusieurs agences ont accées à une plateforme commune.
- Plateforme >- Utilisateurs : 1 to many.
    - Plateforme peut acceuillir des nouveaux utilisateurs.
- Plateforme >- UtilisateursProduits : 1 to many.
    - Plateforme à des utilisateurs qui ont déjà commandés.
- Plateforme >- Catalogue : 1 to 1.
    - Plateforme à accée a un catalogue.
- Catalogue >- Produits : 1 to many.
    - Le catalogue propose plusieurs produits.
- Catalogue >- Services : 1 to many.
    - Le catalogue propose plusieurs services.
- Libéllé >- Produits : many to many.
- Libéllé >- Services : many to many.
- Agents >- Catalogue : 1 to 1.
    - Un agent fait référence a un produit ou service.
- Agents >- UtilisateursProduits : 1 to 1.
    - L'agent attribus une agence a l'utilisateur.
- Agents >- AgencesFrance : 1 to many.
    - Plusieurs agents pour agence.

![](./Contest-MCD.PNG)
